

import UIKit

class ArtistVC: UIViewController,UITableViewDelegate,UITableViewDataSource
{
   
    @IBOutlet weak var tableview: UITableView!
    var artists = [ArtistModel]()
    
    override func viewDidLoad() {
       

        self.tableview.delegate = self
        self.tableview.dataSource = self
        
         super.viewDidLoad()
        

        
        if let json = self.readJSONFromFile(fileName:"artist")
        {
            self.artists = ArtistModel().getArtist(list: json as! [[String : Any]])
            self.artists.sort(by: { $0.artistName < $1.artistName})
            
            DispatchQueue.main.async {
                self.tableview.reloadData()
            }
        }
    }

    
    func  readJSONFromFile(fileName: String) -> Any {
        var json: Any?
        if let path = Bundle.main.path(forResource: fileName, ofType: "json"){
            do{
                let  fileUrl  = URL(fileURLWithPath: path)
                let data = try Data(contentsOf: fileUrl, options: .mappedIfSafe)
                json = try? JSONSerialization.jsonObject(with: data)
            }catch{
                
            }
        }
        return json
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.artists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ArtistCell
        let artist = self.artists[indexPath.row]
        cell.configureResultCell(with: artist)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 176
    }
    
}
