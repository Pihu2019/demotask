

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailTxtFld: UITextField!
    
    @IBOutlet weak var passwordTxtFld: UITextField!
    
    @IBOutlet weak var loginBtnClicked: UIButton!
    
    @IBOutlet weak var registerBtnClicked: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
    
    
        emailTxtFld.delegate = self
        passwordTxtFld.delegate = self
        
        emailTxtFld.textColor = .white
        passwordTxtFld.textColor = .white
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.clear
        ]

    
    }


    @IBAction func loginBtnClicked(_ sender: Any) {
        
//        emailTxtFld.text = UserDefaults.standard.string(forKey: "emailIdTxtFld")
//
//        passwordTxtFld.text = UserDefaults.standard.string(forKey: "passwordTxtFld")
        //setting temp value
        let myUsername: String
        myUsername = emailTxtFld.text!

        let myPass: String
        myPass = passwordTxtFld.text!
        
       if(myUsername == "priya" && myPass == "12345"){
       // if((emailTxtFld!.text != nil)  && ((passwordTxtFld?.text) != nil)){
            print("login success")
//            let alert = UIAlertController(title: "Hi", message: "Login successfully", preferredStyle: UIAlertController.Style.alert)
//            alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler:nil))
//            self.present(alert, animated: true, completion:  nil)
        
       
        let admin = storyboard?.instantiateViewController(withIdentifier: "showList") as? ListViewScreen
        
        if let admin = admin {
            navigationController?.pushViewController(admin, animated: true)
        }

            
        }
        else if(myUsername.isEmpty && myPass.isEmpty)
        {
            let alert = UIAlertController(title: "Alert", message: "Please fill all fields", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler:nil))
            self.present(alert, animated: true, completion:  nil)

        }
        else{
            
            let alert = UIAlertController(title: "Oops", message: "Your account is not registered", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertAction.Style.default, handler:nil))
            self.present(alert, animated: true, completion:  nil)
        }
        
    }
}


////  Converted to Swift 5 by Swiftify v5.0.23302 - https://objectivec2swift.com/
//func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//    
//    
//    if (segue.identifier == "showList") {
//        
//        let wvc = segue.destination as? ListViewScreen
//    }
//}
