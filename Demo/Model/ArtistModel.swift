

import Foundation
import UIKit

struct ArtistModel {
    var wrapperType = ""
    var artistType = ""
    var artistName = ""
    var artistLinkUrl = ""
    var primaryGenName = ""
    var artistId = ""
    
    func getArtist(list: [[String: Any]]) -> [ArtistModel] {
        var artists = [ArtistModel]()
        for data in list
        {
            artists.append(ArtistModel().getArtist(list: data))
        }
        return artists
    }
    
    func getArtist(list: [String: Any]) -> ArtistModel {
        var artist = ArtistModel()
        
         artist.wrapperType = list["wrapperType"] as? String ?? ""
         artist.artistType = list["artistType"] as? String ?? ""
         artist.artistName = list["artistName"] as? String ?? ""
         artist.artistLinkUrl = list["artistLinkUrl"] as? String ?? ""
         artist.primaryGenName = list["primaryGenreName"] as? String ?? ""
         artist.artistId = list["artistId"] as? String ?? ""
        
        return artist
        
        
    }
}
