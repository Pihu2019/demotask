//
//  List.swift
//  Demo
//
//  Created by shayam on 05/06/19.
//  Copyright © 2019 shayam. All rights reserved.
//

import Foundation
import UIKit  //when u use imageview

class  List {
    var image: UIImage
    var  title: String
    
    init(image: UIImage, title: String) {
        self.image = image
        self.title = title
    }
    
}
