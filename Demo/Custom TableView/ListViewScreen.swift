//
//  ListViewScreen.swift
//  Demo
//
//  Created by shayam on 05/06/19.
//  Copyright © 2019 shayam. All rights reserved.
//

import UIKit

class ListViewScreen: UIViewController {
   
    @IBOutlet weak var tableView: UITableView!
    var lists: [List] = []//Initilizing empty array
    
    override func viewDidLoad() {
        super.viewDidLoad()

       lists = createArray()
        
       tableView.delegate = self
       tableView.dataSource = self
    }
    
    func  createArray() -> [List] {
        
        var tempList: [List] = []
        
        let list1 = List(image: UIImage(named:"one")!, title: "one")
        let list2 = List(image: UIImage(named:"two")!, title: "two")
        let list3 = List(image: UIImage(named:"three")!, title: "three")
        let list4 = List(image: UIImage(named:"four")!, title: "four")
        let list5 = List(image: UIImage(named:"five")!, title: "five")
        
        tempList.append(list1)
        tempList.append(list2)
        tempList.append(list3)
        tempList.append(list4)
        tempList.append(list5)
        
        return tempList
        
        
    }
}
extension ListViewScreen: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let list = lists[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as! ListCell
        
        cell.setList(list: list)
        
        return cell
    }
}
