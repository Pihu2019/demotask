//
//  ListCell.swift
//  Demo
//
//  Created by shayam on 05/06/19.
//  Copyright © 2019 shayam. All rights reserved.
//

import UIKit

class ListCell: UITableViewCell {

    @IBOutlet weak var llistimgView: UIImageView!
    
    @IBOutlet weak var listTitleLbl: UILabel!
    
    func setList(list: List) {
        llistimgView.image = list.image
        listTitleLbl.text = list.title
    }
}
