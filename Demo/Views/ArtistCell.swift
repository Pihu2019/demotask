

import UIKit

class ArtistCell: UITableViewCell {

   
    @IBOutlet weak var wrapperType: UILabel!
    @IBOutlet weak var artistType: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var artistLinkUrl: UILabel!
    @IBOutlet weak var primaryGenreName: UILabel!
       @IBOutlet weak var artistId: UILabel!
    
    func configureResultCell(with artist:ArtistModel!) {
        self.wrapperType.text = artist?.artistName.capitalized
        self.artistType.text = artist?.artistType
        self.artistName.text = artist?.artistName
        self.artistLinkUrl.text = artist?.artistLinkUrl
        self.primaryGenreName.text = artist?.primaryGenName
        self.artistId.text = artist?.artistId
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
