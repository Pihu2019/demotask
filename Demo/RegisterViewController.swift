

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var registerBtnClicked: UIButton!
    @IBOutlet weak var confirmPassTxtFld: UITextField!
    @IBOutlet weak var passwordTxtFld: UITextField!
    @IBOutlet weak var emailIdTxtFld: UITextField!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        emailIdTxtFld.delegate = self as? UITextFieldDelegate
        passwordTxtFld.delegate = self as? UITextFieldDelegate
        confirmPassTxtFld.delegate = self as? UITextFieldDelegate
        
        emailIdTxtFld.textColor = .white
        passwordTxtFld.textColor = .white
        confirmPassTxtFld.textColor = .white
        
    }
    @IBAction func registerBtnClicked(_ sender: Any) {
        UserDefaults.standard.set(emailIdTxtFld.text, forKey: "emailIdTxtFld")
        UserDefaults.standard.set(passwordTxtFld.text, forKey: "passwordTxtFld" )
    }
}
